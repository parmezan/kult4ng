package michalz.foundryvtt.api

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

@js.native
@JSGlobalScope
object FoundryGlobal extends js.Object {
  def loadTemplates(templates: js.Array[String]): js.Any = js.native
  def mergeObject(original: js.Any, other: js.Any): js.Dictionary[js.Any] = js.native
}
