package michalz.foundryvtt.api.application

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
trait Application extends js.Object {

}

@js.native
@JSGlobal
object Application extends js.Object {
  def defaultOptions: js.Dictionary[js.Any] = js.native
}
