package michalz.foundryvtt.api.application

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
trait FormApplication extends Application {

}

@js.native
@JSGlobal
object FormApplication extends js.Object {
  def defaultOptions: js.Dictionary[js.Any] = js.native
}