package michalz.foundryvtt.api

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
object Hooks extends js.Object {

  def once(callbackName: String, fun: js.Function): Unit = js.native

}
