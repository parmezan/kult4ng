package michalz.foundryvtt.api.item

import michalz.foundryvtt.api.base.Entity

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class Item(inputData: js.Any, options: js.UndefOr[js.Any]) extends Entity {
  override def data: ItemData  = js.native
  def sheet:         ItemSheet = js.native
}
