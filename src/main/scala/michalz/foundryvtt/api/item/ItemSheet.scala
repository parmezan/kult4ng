package michalz.foundryvtt.api.item

import michalz.foundryvtt.api.base.BaseEntitySheet

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class ItemSheet(item: Item, options: js.Dictionary[js.Any] = js.Dictionary.empty) extends BaseEntitySheet {

  def item: Item = js.native
  def getData(): js.Dictionary[js.Any] = js.native
}

@js.native
@JSGlobal
object ItemSheet extends js.Object {

  def defaultOptions: js.Dictionary[js.Any] = js.native

}