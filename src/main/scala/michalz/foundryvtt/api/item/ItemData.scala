package michalz.foundryvtt.api.item

import michalz.foundryvtt.api.base.EntityData

import scala.scalajs.js

@js.native
trait ItemData extends EntityData {
}
