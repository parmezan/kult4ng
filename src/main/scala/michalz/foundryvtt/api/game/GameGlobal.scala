package michalz.foundryvtt.api.game

import michalz.foundryvtt.api.utils.FoundryMap

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("game")
object GameGlobal extends js.Object{
  def kult4NG: js.UndefOrOps[Kult4NGGameObject] = js.native
  def kult4NG_=(k: Kult4NGGameObject): Unit = js.native
  def packs: FoundryMap = js.native
}

