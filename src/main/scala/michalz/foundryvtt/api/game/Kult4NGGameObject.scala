package michalz.foundryvtt.api.game

import scala.scalajs.js

class Kult4NGGameObject(actorClass: js.Dynamic, itemClass: js.Dynamic) extends js.Object {
  def Kult4NGActor: js.Dynamic = actorClass
  def Kult4NGItem: js.Dynamic = itemClass
}
