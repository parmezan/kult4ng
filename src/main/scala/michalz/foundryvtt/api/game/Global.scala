package michalz.foundryvtt.api.game

import michalz.foundryvtt.api.base.compendium.Compendium

object Global {

  def findCompendium(compendiumKey: String): Option[Compendium] = {
    Option(GameGlobal.packs.get(compendiumKey)).map(_.asInstanceOf[Compendium])
  }

}
