package michalz.foundryvtt.api.config

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@JSGlobal
@js.native
object CONFIG extends js.Object {
  def Actor: ActorConfig = js.native
  def Item: ItemConfig = js.native
}
