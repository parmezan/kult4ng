package michalz.foundryvtt.api.config

import scala.scalajs.js

@js.native
trait ActorConfig extends js.Object {
  def entityClass: js.Dynamic = js.native
  def entityClass_=(actorClass: js.Dynamic): Unit = js.native
}
