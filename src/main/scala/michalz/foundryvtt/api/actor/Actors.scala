package michalz.foundryvtt.api.actor

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
object Actors extends js.Object{
  def unregisterSheet(module: String, sheetClass: js.Dynamic): js.Any = js.native
  def registerSheet(module: String, sheetClass: js.Dynamic, params: js.Dictionary[js.Object]): js.Any = js.native
}
