package michalz.foundryvtt.api.actor

import michalz.foundryvtt.api.base.EntityData

import scala.scalajs.js

@js.native
trait ActorData extends EntityData {

}
