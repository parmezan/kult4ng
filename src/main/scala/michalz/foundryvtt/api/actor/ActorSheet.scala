package michalz.foundryvtt.api.actor

import io.udash.wrappers.jquery.JQuery
import michalz.foundryvtt.api.base.BaseEntitySheet

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class ActorSheet(actor: Actor, options: js.Dictionary[js.Any] = js.Dictionary.empty) extends BaseEntitySheet {
  def id: String = js.native
  def actor: Actor = js.native
  def getData(): ActorSheetData = js.native
  def activateListeners(html: JQuery): Unit = js.native
}

@js.native
@JSGlobal
object ActorSheet extends js.Object {
  def defaultOptions: js.Dictionary[js.Any] = js.native
}