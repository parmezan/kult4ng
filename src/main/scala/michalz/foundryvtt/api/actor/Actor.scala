package michalz.foundryvtt.api.actor

import michalz.foundryvtt.api.base.Entity
import michalz.foundryvtt.api.item.{Item, ItemData}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal
import scala.scalajs.js.JSConverters._

@js.native
@JSGlobal
class Actor(inputData: js.Any, options: js.UndefOr[js.Any]) extends Entity {
  def deleteEmbeddedEntity(embeddedName: String, ids: js.Array[String], options: js.Dictionary[js.Any] = js.Dictionary.empty): js.Promise[js.Object] = js.native
  def createOwnedItem(itemData: js.Dictionary[js.Any], options: js.Dictionary[_ <: js.Any]): js.Promise[ItemData] = js.native
  def getOwnedItem(itemId: String): js.UndefOr[Item] = js.native
  def sheet: ActorSheet = js.native
  override def data: ActorData = js.native
}

object Actor {
  implicit class RichActor(self: Actor) {
    def createOwnedItem(`type`: String, name: Option[String] = None,  renderSheet: Boolean = false, additionalData: Map[String, js.Any] = Map.empty): js.Promise[ItemData] = {
      val data: js.Dictionary[js.Any] = (Map(
        "name" -> js.Object(name.getOrElse(s"New ${`type`}")),
        "type" -> js.Object(`type`)
      ) ++ additionalData).toJSDictionary
      val options = js.Dictionary(
        "renderSheet" -> js.Object(renderSheet)
      )
      self.createOwnedItem(data, options)
    }
  }
}
