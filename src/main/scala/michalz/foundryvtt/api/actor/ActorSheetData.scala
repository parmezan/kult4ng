package michalz.foundryvtt.api.actor

import michalz.foundryvtt.api.item.ItemData

import scala.scalajs.js

@js.native
trait ActorSheetData extends js.Object {
  def data: js.Object = js.native
  def items: js.Array[ItemData] = js.native
  def actor: Actor = js.native
}
