package michalz.foundryvtt.api.utils

import scala.scalajs.js

@js.native
trait FoundryMap extends js.Object {
  def get(key: String): js.Any = js.native
}
