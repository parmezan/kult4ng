package michalz.foundryvtt.api.base

import scala.scalajs.js

@js.native
trait EntityData extends js.Object {
  def _id:    String    = js.native
  def data:   js.Object = js.native
  def name:   String    = js.native
  def `type`: String    = js.native
}
