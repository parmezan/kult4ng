package michalz.foundryvtt.api.base

import michalz.foundryvtt.api.application.FormApplication

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
trait BaseEntitySheet extends FormApplication {

  def render(force: Boolean, options: js.Dictionary[js.Any] = js.Dictionary.empty): js.Promise[Unit] = js.native

}

@js.native
@JSGlobal
object BaseEntitySheet extends js.Object {
  def defaultOptions: js.Dictionary[js.Any] = js.native
}
