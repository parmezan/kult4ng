package michalz.foundryvtt.api.base.compendium

import michalz.foundryvtt.api.base.Entity

import scala.scalajs.js

@js.native
trait Compendium extends js.Object {
  def getIndex(): js.Promise[js.Array[CompendiumIndexEntry]] = js.native
  def getEntity(entryId: String): js.Promise[Entity] = js.native
}
