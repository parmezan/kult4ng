package michalz.foundryvtt.api.base.compendium

import scala.scalajs.js

@js.native
trait CompendiumIndexEntry extends js.Object {
  def _id: String = js.native
  def name: String = js.native
  def img: String = js.native
}
