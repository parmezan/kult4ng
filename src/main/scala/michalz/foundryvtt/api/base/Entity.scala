package michalz.foundryvtt.api.base

import scala.scalajs.js

@js.native
trait Entity extends js.Object {
  def id:   String    = js.native
  def data: js.Object = js.native
  def name: String    = js.native
}
