package michalz.foundryvtt.api.roll

import org.scalajs.dom.raw.HTMLElement

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class Roll(formula: String, data: js.Dictionary[js.Any] = js.Dictionary.empty) extends js.Object {

  def total: Int = js.native

  def roll(): Roll = js.native
  def render(): js.Promise[HTMLElement] = js.native

}