package michalz.foundryvtt.api.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("ui")
object UiGlobal extends js.Any {
  def notifications: Notifications = js.native
}
