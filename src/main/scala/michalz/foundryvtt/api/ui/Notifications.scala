package michalz.foundryvtt.api.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
trait Notifications extends js.Any {
  def info(message: String, options: js.Dictionary[js.Any] = js.Dictionary.empty): Unit = js.native
  def warn(message: String, options: js.Dictionary[js.Any] = js.Dictionary.empty): Unit = js.native
  def error(message: String, options: js.Dictionary[js.Any] = js.Dictionary.empty): Unit = js.native
}
