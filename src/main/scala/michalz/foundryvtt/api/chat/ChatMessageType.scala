package michalz.foundryvtt.api.chat

case class ChatMessageType(value: Int) extends AnyVal

object ChatMessageType {
  val other = ChatMessageType(0)
  val outOfCharacter = ChatMessageType(1)
  val inCharacter = ChatMessageType(2)
  val emote = ChatMessageType(3)
  val wishper = ChatMessageType(4)
  val roll = ChatMessageType(5)
}