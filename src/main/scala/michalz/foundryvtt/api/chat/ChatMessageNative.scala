package michalz.foundryvtt.api.chat

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExportStatic, JSGlobal}

@js.native
@JSGlobal("ChatMessage")
class ChatMessageNative(params: js.Object) extends js.Object {

}

@js.native
@JSGlobal("ChatMessage")
object ChatMessageNative extends js.Object {

  def create(params: js.Dictionary[_ <: js.Any]): js.Promise[ChatMessageNative] = js.native
  def getSpeaker(params: js.Dictionary[_ <: js.Any]): js.Object = js.native

}
