package michalz.foundryvtt.api.chat

import michalz.foundryvtt.api.roll.Roll
import cats.syntax.option._

import scala.scalajs.js
import scala.scalajs.js.JSConverters._

object ChatMessage {

  def create(content: String, messageType: ChatMessageType = ChatMessageType.other, roll: Option[Roll] = none, speaker: Option[js.Object] = none): js.Promise[ChatMessageNative] = {
    val params = List(
      ("content" -> js.Object(content)).some,
      ("type" -> js.Object(messageType.value)).some,
      roll.map("roll" -> js.Object(_)),
      speaker.map("speaker" -> js.Object(_))
    ).flatten.toMap.toJSDictionary

    ChatMessageNative.create(params)
  }

  def speakerByAlias(alias: String): js.Object =
    ChatMessageNative.getSpeaker(js.Dictionary("alias" -> alias))


}
