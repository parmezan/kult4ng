package michalz.foundryvtt.kult4e.actor

import scala.scalajs.js

@js.native
trait Kult4NGActorData extends js.Object {
  def attributes: js.Dictionary[Int] = js.native
}
