package michalz.foundryvtt.kult4e.actor

import cats.data.EitherT
import cats.syntax.option._
import cats.syntax.traverse._
import io.udash.wrappers.jquery.{jQ, EventName, JQuery}
import michalz.foundryvtt.api.FoundryGlobal
import michalz.foundryvtt.api.actor.{ActorSheet, ActorSheetData}
import michalz.foundryvtt.api.chat.{ChatMessage, ChatMessageType}
import michalz.foundryvtt.api.game.Global
import michalz.foundryvtt.api.item.ItemData
import michalz.foundryvtt.api.roll.Roll
import michalz.foundryvtt.api.ui.UiGlobal
import michalz.foundryvtt.kult4e.actor.Kult4NGActorSheet.{moveLikeTypes, movesPackId, updateOptions}
import michalz.foundryvtt.kult4e.item.{BaseRollData, Kult4NGItem, MoveRollData}
import org.scalajs.dom.{console, Element}

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js
import scala.scalajs.js.JSConverters._
import scala.scalajs.js.annotation.JSExportStatic
import scala.util.{Failure, Success}

class Kult4NGActorSheet(actor: Kult4NGActor, options: js.Dictionary[js.Any] = js.Dictionary.empty)
    extends ActorSheet(actor, updateOptions(actor, options)) {

  override def getData(): ActorSheetData = {
    val data = super.getData().asInstanceOf[Kult4NGActorSheetData]
    data.moves = data.items.filter(itemData => moveLikeTypes.contains(itemData.`type`))
    console.log(data)
    data
  }

  override def activateListeners(html: JQuery): Unit = {
    super.activateListeners(html)

    html
      .find(".item-delete")
      .on(
        EventName.click, { (element, event) =>
          val parentElement = jQ(element).parents(".item-name")
          val itemId        = parentElement.data("itemId").map(_.toString)
          actor
            .deleteEmbeddedEntity("OwnedItem", js.Array(itemId.toList: _*))
            .toFuture
            .map(_.asInstanceOf[ItemData])
            .onComplete {
              case Success(deleted) =>
                UiGlobal.notifications.info(s"Item ${deleted.name} deleted")

              case Failure(exception) =>
                UiGlobal.notifications.warn(s"Can't delete item")
                console.error(exception)

            }
        }
      )

    html
      .find(".item-create")
      .on(EventName.click, { (element, event) =>
        createNewItem(element)
      })

    html
      .find(".item-edit")
      .on(EventName.click, { (elem, event) =>
        editItem(elem)
      })

    html
      .find(".basic-move-roll")
      .on(EventName.click, { (elem, event) =>
        basicRoll(elem.textContent)
      })
  }

  def createNewItem(element: Element) = {
    jQ(element).data("type") match {
      case None =>
        UiGlobal.notifications.warn("Can't create element, no type")
        console.warn(element)

      case Some(newType) =>
        actor.createOwnedItem(newType.toString, None, true).toFuture.onComplete {
          case Success(createdData) =>
            ()

          case Failure(ex) =>
            console.error(ex)
        }
    }
  }

  def editItem(elem: Element) = {
    val parentElement = jQ(elem).parents(".item-name")
    val itemIdOpt     = parentElement.data("itemId").map(_.toString)
    val itemOpt = for {
      itemId <- itemIdOpt
      item <- actor.getOwnedItem(itemId).toOption
    } yield item

    itemOpt match {
      case Some(item) =>
        item.sheet.render(true)

      case None =>
        console.warn(s"Invalid item id ${itemOpt.getOrElse("")} owned by actor ${actor.id}")
    }
  }

  def basicRoll(moveName: String): js.Promise[Unit] = {

    val rollResult = (for {
      movesCompendium <- EitherT.fromEither[Future](
        Global.findCompendium(movesPackId).toRight(s"Can't find compendium: ${movesPackId}"))
      movesIndex <- EitherT.liftF(movesCompendium.getIndex().toFuture)
      moveId <- EitherT
        .fromEither[Future](movesIndex.find(_.name == moveName).toRight(s"No ${moveName} in $movesPackId compendium"))
        .map(_._id)
      moveItem <- EitherT(
        movesCompendium
          .getEntity(moveId)
          .toFuture
          .map(Option(_).toRight(s"Can't get $moveName ($moveId) from compendium"))).map(_.asInstanceOf[Kult4NGItem])
      baseRollData = moveItem.data.data.asInstanceOf[BaseRollData]
      rollResult <- moveRoll(MoveRollData(moveName, moveItem.data.`type`, none, baseRollData))
    } yield rollResult).value

    rollResult.onComplete {
      case Success(Right(_)) => ()

      case Success(Left(value)) =>
        UiGlobal.notifications.error(s"Error during rolling for ${moveName}")
        console.log(value)

      case Failure(ex) =>
        UiGlobal.notifications.error(s"Error during rolling for ${moveName}")
        console.log(ex)
    }

    //Roll
    rollResult.map(_ => ()).toJSPromise
  }

  def moveRoll(moveRollData: MoveRollData): EitherT[Future, String, Unit] = {
    val attrMod = moveRollData.attributemod.traverse { attrModName =>
      actor.kult4NGData.attributes.get(attrModName).toRight(s"No ${attrModName} in actor data")
    }

    val rollE = attrMod.map { attributeMod =>
      val formula = List(
        "2d10".some,
        attributeMod.map(_.toString),
        moveRollData.additionalMod.map(_.toString)
      ).flatten.mkString("+")

      new Roll(formula).roll()
    }

    for {
      roll <- EitherT.fromEither[Future](rollE)
      renderedRoll <- EitherT.liftF(roll.render().toFuture)
      rollResult = moveRollData.renderResult(roll.total)
      msg        = rollResult + renderedRoll
      _ <- EitherT.liftF(
        ChatMessage.create(msg, ChatMessageType.roll, roll.some, ChatMessage.speakerByAlias(actor.name).some).toFuture)
    } yield ()
  }
}

object Kult4NGActorSheet {

  @JSExportStatic
  def name: String = "Kult4NGActorSheet"

  def movesPackId = "kult4ng.moves"

  def moveLikeTypes = Seq(
    "move",
    "advantage",
    "disadvantage",
    "darksecret",
    "relationship"
  )

  @JSExportStatic
  def defaultOptions: js.Dictionary[js.Any] = {

    val options = js.Dynamic.literal(
      template = "systems/kult4ng/templates/sheets/pc-sheet.hbs",
      classes  = List("kult4ng", "actor", "sheet").toJSArray
    )

    FoundryGlobal.mergeObject(ActorSheet.defaultOptions, options)
  }

  def updateOptions(actor: Kult4NGActor, options: js.Dictionary[js.Any]): js.Dictionary[js.Any] = {
    val newOptions = js.Dynamic.literal(title=actor.name)
    FoundryGlobal.mergeObject(options, newOptions)
  }
}
