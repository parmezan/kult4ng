package michalz.foundryvtt.kult4e.actor

import michalz.foundryvtt.api.actor.Actor

import scala.scalajs.js.annotation.{JSExportStatic, JSExportTopLevel}
import scala.scalajs.js

@JSExportTopLevel("Kult4NGActor")
class Kult4NGActor(inputData: js.Any, options: js.UndefOr[js.Any]) extends Actor(inputData, options) {
  def kult4NGData: Kult4NGActorData = data.data.asInstanceOf[Kult4NGActorData]
}

object Kult4NGActor {
  @JSExportStatic
  def name: String = "Kult4NGActorData"
}