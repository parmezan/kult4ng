package michalz.foundryvtt.kult4e.actor

case class Stability(name: String, value: Int)
