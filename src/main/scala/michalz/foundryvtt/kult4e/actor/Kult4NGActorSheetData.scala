package michalz.foundryvtt.kult4e.actor

import michalz.foundryvtt.api.actor.ActorSheetData
import michalz.foundryvtt.api.item.{Item, ItemData}

import scala.scalajs.js

@js.native
trait Kult4NGActorSheetData extends ActorSheetData {
  def moves: js.UndefOr[js.Array[ItemData]] = js.native
  def moves_=(moves: js.Array[ItemData]): Unit = js.native
}
