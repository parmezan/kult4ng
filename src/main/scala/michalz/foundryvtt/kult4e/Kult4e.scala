package michalz.foundryvtt.kult4e

import io.udash.wrappers.jquery.jQ
import michalz.foundryvtt.api.FoundryGlobal.loadTemplates
import michalz.foundryvtt.api.game.{GameGlobal, Kult4NGGameObject}
import michalz.foundryvtt.api.Hooks
import michalz.foundryvtt.api.actor.{ActorSheet, Actors}
import michalz.foundryvtt.api.config.CONFIG
import michalz.foundryvtt.api.item.{ItemSheet, Items}
import michalz.foundryvtt.kult4e.actor.{Kult4NGActor, Kult4NGActorSheet}
import michalz.foundryvtt.kult4e.config.Kult4NGConfig
import michalz.foundryvtt.kult4e.item.{Kult4NGItem, Kult4NGItemSheet}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import scala.scalajs.js.JSConverters._

object Kult4e extends App {

  val templatepaths = Seq(
    "systems/kult4ng/templates/partials/move-card.hbs",
    "systems/kult4ng/templates/partials/darksecret-card.hbs",
    "systems/kult4ng/templates/partials/relationship-card.hbs",
    "systems/kult4ng/templates/partials/weapon-card.hbs",
    "systems/kult4ng/templates/partials/attributeSelect.hbs"
  )

  @JSExport
  val init: js.Function = { () =>
    println("Initializing Kult4NG Scalajs System")
    loadTemplates(templatepaths.toJSArray)

    GameGlobal.kult4NG = new Kult4NGGameObject(js.constructorOf[Kult4NGActor], js.constructorOf[Kult4NGItem])

    CONFIG.Actor.entityClass = js.constructorOf[Kult4NGActor]
    CONFIG.Item.entityClass = js.constructorOf[Kult4NGItem]
    CONFIG.asInstanceOf[js.Dynamic].updateDynamic("kult4ng")(Kult4NGConfig)

    val defaultParams = Map("default" -> js.Object(true)).toJSDictionary
    Items.unregisterSheet("core", js.constructorOf[ItemSheet])
    Items.registerSheet("kult4NG", js.constructorOf[Kult4NGItemSheet], defaultParams)
    Actors.unregisterSheet("core", js.constructorOf[ActorSheet])
    Actors.registerSheet("kult4NG", js.constructorOf[Kult4NGActorSheet], defaultParams)

    ()
  }


  Hooks.once("init", init)
}
