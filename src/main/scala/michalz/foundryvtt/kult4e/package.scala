package michalz.foundryvtt

import org.scalajs.dom.console

package object kult4e {

  def outputLog[A](elem: A, params: Any*): A = {
    console.log(elem, params: _*)
    elem
  }

}
