package michalz.foundryvtt.kult4e.config

import scala.scalajs.js
import scala.scalajs.js.JSConverters._
import scala.scalajs.js.annotation.JSExportTopLevel

@JSExportTopLevel("Kult4NGConfig")
object Kult4NGConfig extends js.Object {

  def name: String = "Kult4NGConfig"

  val attributes: js.Dictionary[String] = Map(
    "willpower" -> "Willpower",
    "fortitude" -> "Fortitude",
    "reflexes" -> "Reflexes",
    "reason" -> "Reason",
    "intuition" -> "Intuition",
    "perception" -> "Perception",
    "coolnes" -> "Coolness",
    "violence" -> "Violence",
    "charisma" -> "Charisma",
    "soul" -> "Soul"
  ).toJSDictionary

}
