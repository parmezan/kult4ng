package michalz.foundryvtt.kult4e.item

import michalz.foundryvtt.api.FoundryGlobal
import michalz.foundryvtt.api.item.ItemSheet
import michalz.foundryvtt.kult4e.config.Kult4NGConfig
import michalz.foundryvtt.kult4e.item.Kult4NGItemSheet.updateOptions
import michalz.foundryvtt.kult4e.outputLog
import org.scalajs.dom.console

import scala.scalajs.js
import scala.scalajs.js.Dictionary
import scala.scalajs.js.JSConverters._
import scala.scalajs.js.annotation.JSExportStatic

class Kult4NGItemSheet(item: Kult4NGItem, options: js.Dictionary[js.Any] = js.Dictionary.empty)
    extends ItemSheet(item, updateOptions(item, options)) {
  def kultItemData: Kult4NGItemData = getData()("data").asInstanceOf[Kult4NGItemData]

  override def getData(): js.Dictionary[js.Any] = {
    val superData = super.getData()
    val kultData = superData("data").asInstanceOf[Kult4NGItemData]
    val additionalData = js.Dynamic.literal(
      attributesMap = Kult4NGConfig.attributes,
      attributeData = js.Dynamic.literal(
        attributeMod = kultData.attributemod,
        attributesMap = Kult4NGConfig.attributes
      )
    )
    outputLog(FoundryGlobal.mergeObject(superData, additionalData), "Kult4NGItemSheet.getData")
  }

  console.log(this)
}

object Kult4NGItemSheet {

  @JSExportStatic
  def name: String = "Kult4NGItemSheet"

  @JSExportStatic
  def defaultOptions: js.Dictionary[js.Any] = {

    val options = js.Dynamic.literal(
      classes = List("kult4ng", "item", "sheet").toJSArray
    )

    FoundryGlobal.mergeObject(ItemSheet.defaultOptions, options)
  }

  def updateOptions(item: Kult4NGItem, options: js.Dictionary[js.Any]): js.Dictionary[js.Any] = {
    val newOptions = js.Dynamic.literal(
      template = s"systems/kult4ng/templates/sheets/${item.data.`type`}-sheet.hbs"
    )

    outputLog(FoundryGlobal.mergeObject(options, newOptions), "Kult4NGItemSheet.updateOptions")
  }
}
