package michalz.foundryvtt.kult4e.item

import michalz.foundryvtt.api.item.Item

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExportStatic, JSExportTopLevel}

@JSExportTopLevel("Kult4NGItem")
class Kult4NGItem(inputData: js.Any, options: js.UndefOr[js.Any]) extends Item(inputData, options) {
  def kult4NGData: Kult4NGItemData = data.data.asInstanceOf[Kult4NGItemData]
}

object Kult4NGItem {
  @JSExportStatic
  def name: String = "Kult4NGItem"
}
