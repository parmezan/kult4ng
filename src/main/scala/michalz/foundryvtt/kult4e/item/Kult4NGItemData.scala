package michalz.foundryvtt.kult4e.item

import scalajs.js

@js.native
trait Kult4NGItemData extends js.Object {

  def `type`: String = js.native
  def attributemod: js.UndefOr[String] = js.native

}
