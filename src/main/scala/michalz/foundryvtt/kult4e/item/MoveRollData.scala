package michalz.foundryvtt.kult4e.item

import cats.syntax.option._
import scalatags.Text.all._

case class MoveRollData(
    name:            String,
    `type`:          String,
    additionalMod:   Option[Int],
    description:     String,
    attributemod:    Option[String],
    completesuccess: String,
    partialsuccess:  String,
    failure:         String,
    trigger:         String
) {

  def renderResult(rollResult: Int): String = {
    val (description, resultTitle, resultClass) = rollResult match {
      case res if res >= 15 => (completesuccess, "Success", "roll-success")
      case res if res >= 10 => (partialsuccess, "Partial success", "roll-partial-success")
      case _            => (failure, "Failure", "roll-failure")
    }

    div(
      h2(name),
      h3(cls := resultClass)(resultTitle),
      raw(description)
    ).toString()

  }

}

object MoveRollData {
  def apply(name: String, `type`: String, additionalMod: Option[Int], baseRollData: BaseRollData): MoveRollData =
    MoveRollData(
      name            = name,
      `type`          = `type`,
      additionalMod   = additionalMod,
      description     = baseRollData.description,
      attributemod    = baseRollData.attributemod.some,
      completesuccess = baseRollData.completesuccess,
      partialsuccess  = baseRollData.partialsuccess,
      failure         = baseRollData.failure,
      trigger         = baseRollData.trigger
    )

}
