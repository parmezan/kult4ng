package michalz.foundryvtt.kult4e.item

import scala.scalajs.js

@js.native
trait BaseRollData extends js.Object {
  def description: String = js.native
  def attributemod: String = js.native
  def completesuccess: String = js.native
  def partialsuccess: String = js.native
  def failure: String = js.native
  def trigger: String = js.native
}
