This is the first draft of my UNOFFICIAL Kult 4E system for Foundry VTT. It's based on copyrighted material, but contains no copyrighted material. You will be responsible, as the user, for inputting the material from your own copy of the Kult Divinity Lost book. 

This is a PoC to check if scalajs can be used to creating Foundry VTT mods/system.
It is based on [Kult4E Foundry](https://gitlab.com/fattom23/kult4e-foundry) by Tom LaPorta and contains only parts of functionality.

Requirements:
 - JDK 11
 - SBT 1.4.3

Buid and install:
```
$ sbt devDist
```

Output directory: `./target/kult4ng-foundryvtt-system`