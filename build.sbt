ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion := "2.13.4"

lazy val `kult4ng-system` = project
  .in(file("."))
  .settings(
    name := "Kult4NG FoundryVTT System",
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "1.1.0",
      "io.udash" %%% "udash-jquery" % "3.0.4",
      "org.typelevel" %%% "cats-core" % "2.3.1",
      "com.lihaoyi" %%% "scalatags" % "0.9.2"
    ),
//    scalaJSLinkerConfig ~= {
//      _.withESFeatures(_.withUseECMAScript2015(true))
//    },
    scalaJSUseMainModuleInitializer := true,
    includeFilter in (Assets, LessKeys.less) := "*.less"
  )
  .enablePlugins(ScalaJSPlugin)
  .enablePlugins(SbtWeb)
